//
//  ViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 9/19/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit
import ArcGIS
import RxCocoa
import RxSwift

class ViewController: UIViewController {
   static func create(withMapType mapType: AGSBasemapType) -> ViewController {
      let storyboard = UIStoryboard(name: "Main", bundle: .main)
      let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
      vc.baseMapType = mapType
      return vc
   }
   
   var baseMapType: AGSBasemapType!
   var selectedPoint: AGSPoint?
   
   var routeFinalPoint: AGSPoint?
   
   var computedRoute: AGSRoute? {
      didSet {
         routeFinalPoint = self.computedRoute?.routeGeometry?.parts.array().last?.endPoint
      }
   }
   
   @IBOutlet weak var mapView: AGSMapView!
   @IBOutlet weak var routeButton: UIButton!
   @IBOutlet weak var navigateButton: UIButton!
   
   var overlay: AGSGraphicsOverlay!

   override func viewDidLoad() {
      super.viewDidLoad()
      // Do any additional setup after loading the view, typically from a nib.
      mapView.map = AGSMap(basemapType: baseMapType, latitude: 34.057, longitude: -117.196, levelOfDetail: 10)
      
      let fakeDataSource = AGSSimulatedLocationDataSource()

      let polyline = AGSPolyline(
         points: [
            AGSPoint(
               x: -13041735.523091165,
               y: 3857614.9843200161,
               spatialReference: AGSSpatialReference.webMercator()
            )
         ]
      )
   
      fakeDataSource.setLocationsWith(polyline)
      mapView.locationDisplay.dataSource = fakeDataSource
      mapView.locationDisplay.autoPanMode = AGSLocationDisplayAutoPanMode.recenter
      
      mapView.locationDisplay.start { (error) in
         if let error = error {
            print("Error tracking location: \(error)")
         }
      }
      
      mapView.locationDisplay.locationChangedHandler = { [weak self] location in
         guard let `self` = self,
            let position = location.position,
            let route = self.computedRoute else { return }
         
         if position == self.routeFinalPoint {
            self.routeFinalPoint = nil
            let polyline = AGSPolyline(points: [position])
            fakeDataSource.setLocationsWith(polyline)
         }
         
         let maneuver = route.directionManeuvers.first(where: { (maneuver) -> Bool in
            if let point = maneuver.geometry as? AGSPoint {
               return point == location
            }
            else if let polyline = maneuver.geometry as? AGSPolyline {
               return polyline.parts.array().reduce(false, {found, part in
                  for i in 0 ..< part.segmentCount {
                     let segment = part.segment(at: i)
                     
                     if segment.contains(point: position) {
                        return true
                     }
                  }
                  
                  return found
               })
            }
            else {
               print(maneuver.geometry)
               fatalError()
            }
         })
         
         if let maneuver = maneuver {
            print("Next: \(maneuver.maneuverMessages.map {$0.formatted}.joined(separator: ", ")): \(maneuver.directionText)")
         }
      }
      
      overlay = AGSGraphicsOverlay()
      mapView.graphicsOverlays.add(overlay)
      
      mapView.touchDelegate = self
      
      // MARK: - Routes
      let routeOverlay = AGSGraphicsOverlay()
      mapView.graphicsOverlays.add(routeOverlay)
      
      guard let routeServer = URL(
         string: "https://sampleserver6.arcgisonline.com/arcgis/rest/services/NetworkAnalysis/SanDiego/NAServer/Route"
         ) else {
            print("unable to create route server")
            return
      }
      
      
      let routeTask = AGSRouteTask(url: routeServer)
      
      routeTask.load { (error) in
         if let error = error {
            print("Error loading the route: \(error)")
         }
         else if routeTask.loadStatus == .loaded {
            print("route loaded successfully")
         }
      }
      
      var routeParams: AGSRouteParameters!
      
      routeTask.defaultRouteParameters { (params, error) in
         
         if let error = error {
            print("Error getting params: \(error)")
         }
         else {
            routeParams = params
            routeParams.directionsDistanceUnits = .metric
            routeParams.findBestSequence = false
            routeParams.returnDirections = true
            routeParams.returnRoutes = true
            routeParams.routeShapeType = AGSRouteShapeType.trueShapeWithMeasures
            routeParams.outputSpatialReference = AGSSpatialReference.webMercator()
         }
      }
   
      navigateButton.rx.tap.subscribe(onNext: {
         guard let route = self.computedRoute else {
            print("There is no route computed")
            return
         }
         
         if let geometry = route.routeGeometry {
            fakeDataSource.setLocationsWith(geometry)
         }
         
         self.mapView.locationDisplay.autoPanMode = AGSLocationDisplayAutoPanMode.compassNavigation
      }).disposed(by: disposeBag)
      
      routeButton.rx.tap.subscribe(onNext: {
         guard let fromPoint = self.mapView.locationDisplay.mapLocation,
            let toPoint = self.selectedPoint else {
               print("The route requires to points to work")
               return
         }
         
         let fromStop = AGSStop(point: fromPoint)
         fromStop.name = "Origin"
         
         let toStop = AGSStop(point: toPoint)
         toStop.name = "Destination"
         
         routeParams.setStops([fromStop, toStop])
         
         routeOverlay.graphics.removeAllObjects()
         
         routeTask.solveRoute(with: routeParams, completion: { (routeResult, error) in
            if let error = error {
               print("Error solving route: \(error)")
               return
            }
            
            guard let routeResult = routeResult else {
               fatalError()
            }
            
            let routeSymbol = AGSSimpleLineSymbol(style: .solid, color: .green, width: 5)
            
            guard let generatedRoute = routeResult.routes.first else {
               return
            }
            
            self.computedRoute = generatedRoute
         
            let routeGraphic = AGSGraphic(
               geometry: generatedRoute.routeGeometry,
               symbol: routeSymbol,
               attributes: nil
            )
            
            routeOverlay.graphics.add(routeGraphic)
            print("Printing direction: ")
            
            for maneuver in generatedRoute.directionManeuvers {
               print("\(maneuver.maneuverMessages.map {$0.formatted}.joined(separator: ", ")): \(maneuver.directionText)")
            }
         })
      }).disposed(by: disposeBag)
   }
   
   func addPoint(x: Double, y: Double, overlay: AGSGraphicsOverlay) {
      let image = #imageLiteral(resourceName: "map-marker")
      let symbol = AGSPictureMarkerSymbol(image: image)
      symbol.offsetY = image.size.height / 2
      
      let point = AGSPoint(x: x, y: y, spatialReference: AGSSpatialReference.webMercator())
      
      let graphic = AGSGraphic(geometry: point, symbol: symbol, attributes: nil)
      overlay.graphics.add(graphic)
   }
}

extension ViewController: AGSGeoViewTouchDelegate {
   func geoView(_ geoView: AGSGeoView, didTapAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
      
      overlay.graphics.removeAllObjects()
      addPoint(x: mapPoint.x, y: mapPoint.y, overlay: overlay)
      self.selectedPoint = mapPoint
   }
}

extension AGSSegment {
   func contains(point: AGSPoint) -> Bool {
      let length = startPoint.distance(to: endPoint)
      let d1 = startPoint.distance(to: point)
      let d2 = endPoint.distance(to: point)
      return abs(length - d1 - d2) <= 1e-4
   }
}

extension AGSPoint {
   func distance(to point: AGSPoint) -> Double {
      return sqrt(pow(x - point.x, 2) + pow(y - point.y, 2))
   }
}

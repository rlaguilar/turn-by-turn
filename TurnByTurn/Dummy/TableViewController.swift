//
//  TableViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 9/22/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit
import ArcGIS

class TableViewController: UITableViewController {
   let items: [AGSBasemapType] = [
      .imagery, .imageryWithLabels, .streets, .topographic, .terrainWithLabels,
      .lightGrayCanvas, .nationalGeographic, .oceans, .openStreetMap,
      .imageryWithLabelsVector, .streetsVector, .topographicVector,
      .terrainWithLabelsVector, .lightGrayCanvasVector, .navigationVector,
      .streetsNightVector, .streetsWithReliefVector, .darkGrayCanvasVector
   ]
   
   let labels: [String] = [
      "imagery", "imageryWithLabels", "streets", "topographic", "terrainWithLabels",
      "lightGrayCanvas", "nationalGeographic", "oceans", "openStreetMap",
      "imageryWithLabelsVector", "streetsVector", "topographicVector",
      "terrainWithLabelsVector", "lightGrayCanvasVector", "navigationVector",
      "streetsNightVector", "streetsWithReliefVector", "darkGrayCanvasVector"
   ]
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      // Uncomment the following line to preserve selection between presentations
      // self.clearsSelectionOnViewWillAppear = false
      
      // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
      // self.navigationItem.rightBarButtonItem = self.editButtonItem()
      
      tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
   }
   
   // MARK: - Table view data source
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      // #warning Incomplete implementation, return the number of rows
      return items.count
   }
   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
      
      // Configure the cell...
      cell.textLabel?.text = "\(labels[indexPath.row])"
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let baseMapType = items[indexPath.row]
      let storyboard = UIStoryboard(name: "Main", bundle: .main)
      let vc = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
      vc.baseMapType = baseMapType
      navigationController?.pushViewController(vc, animated: true)
   }
}

//
//  Extensions.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 9/28/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import ArcGIS

extension AGSDirectionManeuverType: CustomStringConvertible {
   public var description: String {
      switch self {
      case .bearLeft:
         return "bear Left"
      case .bearRight:
         return "bear right"
      case .depart:
         return "depart"
      case .doorPassage:
         return "door passage"
      case .elevator:
         return "elevator"
      case .endOfFerry:
         return "endOfFerry"
      case .escalator:
         return "escalator"
      case .ferry:
         return "ferry"
      case .forkCenter:
         return "forkCenter"
      case .forkLeft:
         return "forkLeft"
      case .forkRight:
         return "forkRight"
      case .highwayChange:
         return "highwayChange"
      case .highwayExit:
         return "highwayExit"
      case .highwayMerge:
         return "highwayMerge"
      case .pedestrianRamp:
         return "pedestrianRamp"
      case .rampLeft:
         return "rampLeft"
      case .rampRight:
         return "rampRight"
      case .roundabout:
         return "roundabout"
      case .sharpLeft:
         return "sharpLeft"
      case .sharpRight:
         return "sharpRight"
      case .stairs:
         return "stairs"
      case .stop:
         return "stop"
      case .straight:
         return "straight"
      case .tripItem:
         return "tripItem"
      case .turnLeft:
         return "turnLeft"
      case .turnLeftLeft:
         return "turnLeftLeft"
      case .turnLeftRight:
         return "turnLeftRight"
      case .turnRight:
         return "turnRight"
      case .turnRightLeft:
         return "turnLeftLeft"
      case .turnRightRight:
         return "turnLeftRight"
      case .unknown:
         return "unknown"
      case .uTurn:
         return "uTurn"
      }
   }
}

extension AGSDirectionMessage {
   var formatted: String {
      return "\(type):\(text)"
   }
}

extension AGSDirectionMessageType: CustomStringConvertible {
   public var description: String {
      switch self {
      case .alternativeName:
         return "Alternative name"
      case .branch:
         return "branch"
      case .crossStreet:
         return "cross street"
      case .cumulativeLength:
         return "cumulative Length"
      case .estimatedArrivalTime:
         return "estimated arrival time"
      case .exit:
         return "exit"
      case .length:
         return "length"
      case .serviceTime:
         return "service time"
      case .streetName:
         return "street name"
      case .summary:
         return "summary"
      case .time:
         return "time"
      case .timeWindow:
         return "time window"
      case .toward:
         return "toward"
      case .violationTime:
         return "violation time"
      case .waitTime:
         return "wait time"
      }
   }
}

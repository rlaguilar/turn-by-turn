//
//  UserCredentials.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import Mapper

struct UserCredentials: Codable {
   let authToken: String
   
   private enum CodingKeys: String, CodingKey {
      case authToken = "access_token"
   }
}

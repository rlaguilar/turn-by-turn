//
//  SizeupStory.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation

struct SizeupStory: Codable {
   let story: String?
   let url: URL
}

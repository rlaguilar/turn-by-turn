//
//  FailedResponse.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation

struct FailedResponse: Codable {
   let code: Int
   let message: String
}

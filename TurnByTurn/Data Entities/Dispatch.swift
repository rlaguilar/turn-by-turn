//
//  Dispatch.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import Mapper
import CoreLocation

struct Dispatch: Codable {
   let id: Int
   let createdAt: Date
   
   let dispatchType: String?
   let address: String?
   let addressId: Int?
   let latitude: Double?
   let longitude: Double?
   let url: URL?
   let city: String?
   
   var coordinates: CLLocationCoordinate2D? {
      guard let lat = latitude, let lng = longitude else {
         return nil
      }
      
      return CLLocationCoordinate2D(latitude: lat, longitude: lng)
   }

   let dispatchUnits: [String]
   
   let fullMessage: String?
   
   init(from decoder: Decoder) throws {
      let values = try decoder.container(keyedBy: CodingKeys.self)
      
      id = try values.decode(Int.self, forKey: .id)
      createdAt = try values.decode(Date.self, forKey: .createdAt)
      dispatchType = try? values.decode(String.self, forKey: .dispatchType)
      address = try? values.decode(String.self, forKey: .address)
      addressId = try? values.decode(Int.self, forKey: .addressId)
      latitude = try? values.decode(Double.self, forKey: .latitude)
      longitude = try? values.decode(Double.self, forKey: .longitude)
      url = try? values.decode(URL.self, forKey: .url)
      city = try? values.decode(String.self, forKey: .city)
      
      if let units = try? values.decode([String].self, forKey: .dispatchUnits) {
         dispatchUnits = units
      }
      else {
         dispatchUnits = []
      }
      
      fullMessage = try? values.decode(String.self, forKey: .fullMessage)
   }
   
   private enum CodingKeys: String, CodingKey {
      case id, dispatchType = "dispatch_type", createdAt = "created_at", address, addressId = "address_id", latitude, longitude, url, city, dispatchUnits = "dispatch_unit_codes", fullMessage = "full_msg"
   }
}

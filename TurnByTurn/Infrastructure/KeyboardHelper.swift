//
//  KeyboardHelper.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/14/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class KeyboardHelper {
   let disposeBag = DisposeBag()
   
   private let subject = BehaviorSubject<KeyboardState>(value: .none)
   
   lazy var currentKeyboardState: Driver<KeyboardState> = {
      return self.subject.asDriver(onErrorJustReturn: .none)
   }()
   
   init() {
      let center = NotificationCenter.default
      
      Observable.of(
         center.rx.notification(.UIKeyboardWillShow).map { ($0.userInfo, true) },
         center.rx.notification(.UIKeyboardWillHide).map { ($0.userInfo, false) }
         )
         .merge()
         .map { KeyboardState(userInfo: $0, isVisible: $1) }
         .bind(to: subject)
         .disposed(by: disposeBag)
   }
}

struct KeyboardState {
   private let userInfo: [AnyHashable: Any]
   
   let isVisible: Bool
   
   init(userInfo: [AnyHashable: Any]?, isVisible: Bool) {
      self.userInfo = userInfo ?? [:]
      self.isVisible = isVisible
   }
   
   func intersectionHeight(with view: UIView) -> CGFloat {
      guard let frame = self.userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect else {
         return 0
      }
      
      let convertedFrame = view.convert(frame, from: nil)
      let intersection = convertedFrame.intersection(view.bounds)
      return intersection.height
   }
   
   static let none = KeyboardState(userInfo: nil, isVisible: false)
}

//
//  DispatchFetcher.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation

import Reachability
import RxCocoa
import RxSwift

class DispatchFetcher {
   var timeline: Observable<[Dispatch]>
   
   let disposeBag = DisposeBag()
   
   init(apiService: ApiServiceType, pageSize: Int) {
      // FIXME: Listen for remote notifications here!!!
      
      let triggerObservable = Observable.of(
         Observable<Int>.timer(30, period: 30, scheduler: MainScheduler.instance).map { _ in },
         NotificationCenter.default.rx.notification(
            Notification.Name.UIApplicationDidBecomeActive
            ).map { _ in }
//         NotificationCenter.default.rx.notification(
//            RemoteNotificationArrived
//         )
         ).merge()
         .debounce(0.5, scheduler: MainScheduler.instance)
         .startWith(())
      
      let feedCursor = Variable<TimelineCursor>(.none)
      
      let allItems: Observable<[Dispatch]> = Observable.combineLatest(
         triggerObservable,
         Reactive<Reachability>.reachable
      ) { ($0, $1) }
         .filter { (_, reachable) in reachable }
         .map { (trigger, _) in trigger }
         .withLatestFrom(feedCursor.asObservable())
         .flatMapLatest { cursor -> Observable<[Dispatch]> in
            return apiService.getDispatches(
               afterId: cursor.maxId,
               beforeId: nil,
               count: pageSize
            )
         }
         .share(replay: 1, scope: SubjectLifetimeScope.forever)
      
      timeline = allItems.map { items in
         let prev24Hours = Date().addingTimeInterval(-24*60*60)
         return items.filter { $0.createdAt >= prev24Hours }
      }
      
      allItems.scan(TimelineCursor.none) { (currentCursor, alerts) -> TimelineCursor in
         return alerts.reduce(currentCursor, { (cursor, alert) -> TimelineCursor in
            return TimelineCursor(maxId: max(cursor.maxId, alert.id))
         })
         }.bind(to: feedCursor).disposed(by: disposeBag)
   }
}

struct TimelineCursor {
   let maxId: Int
   
   // TODO: Integrate since Id
   static let none = TimelineCursor(maxId: 0)
}


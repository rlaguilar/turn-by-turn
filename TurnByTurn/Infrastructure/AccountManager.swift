//
//  AccountManager.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

typealias LiveAuthStatus = Observable<AuthorizationStatus>

enum AuthorizationStatus {
   case unauthorized
   case autorized(UserCredentials)
   
   var credentials: UserCredentials? {
      switch self {
      case .unauthorized:
         return nil
      case .autorized(let credentials):
         return credentials
      }
   }
}

protocol AccountManagerType {
   var liveAuthStatus: LiveAuthStatus { get }
   var authStatus: AuthorizationStatus { get set }
   
   func logout() -> Observable<Void>
}

class AccountManager: AccountManagerType {
   private var dataManager: DataManagerType
   
   private let _liveAuth = ReplaySubject<AuthorizationStatus>.create(bufferSize: 1)
   
   lazy var liveAuthStatus: LiveAuthStatus = self._liveAuth.asObservable()
   
   var authStatus: AuthorizationStatus {
      get {
         if let credentials = dataManager.credentials {
            return AuthorizationStatus.autorized(credentials)
         }
         else {
            return AuthorizationStatus.unauthorized
         }
      }
      set {
         let credentials = newValue.credentials
         dataManager.credentials = credentials
         _liveAuth.onNext(self.authStatus)
      }
   }
   
   init(dataManager: DataManagerType) {
      self.dataManager = dataManager
      _liveAuth.onNext(self.authStatus)
   }
   
   func logout() -> Observable<Void> {
      // TODO: Stop receiving notifications
//      self.apiService.stopNotifications(for: "")
      
      authStatus = .unauthorized
      dataManager.lastReadDispatch = nil
      return Observable.just(())
   }
}

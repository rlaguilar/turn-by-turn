//
//  Theme.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/10/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import UIKit

class Theme {
   static let `default` = Theme()
   
   func apply() {
      let navBar = UINavigationBar.appearance()
      navBar.barStyle = .black
      navBar.tintColor = .white
   }
}

//
//  AppNotifications.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/10/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import RxSwift

protocol AppNotification {
   associatedtype Payload
   
   static var name: Notification.Name { get }
   var userInfo: [AnyHashable: Any] { get }
   
   init(payload: Payload)
   
   static func parse(userInfo: [AnyHashable: Any]) -> Payload
}

extension AppNotification {
   static var name: Notification.Name { return Notification.Name("\(self)") }
   
   static var observe: Observable<Payload> {
      return NotificationCenter.default.rx.notification(Self.name)
         .map { $0.userInfo ?? [:] }
         .map(Self.parse(userInfo:))
   }
   
   func post(sender: Any? = nil) {
      NotificationCenter.default.post(
         name: Self.name,
         object: sender,
         userInfo: userInfo
      )
   }
   
   static func post(payload: Payload, sender: Any?) {
      let item = self.init(payload: payload)
      item.post(sender: sender)
   }
}

struct OpenUrlNotification: AppNotification {
   private static let urlKey = "url"
   
   var userInfo: [AnyHashable : Any]
   
   static func parse(userInfo: [AnyHashable: Any]) -> URL {
      guard let url = userInfo[self.urlKey] as? URL else {
         fatalError()
      }
      
      return url
   }
   
   init(payload url: URL) {
      userInfo = [OpenUrlNotification.urlKey: url]
   }
}

struct RollToDispatchNotification: AppNotification {
   private static let dispatchKey = "dispatch"
   
   var userInfo: [AnyHashable : Any]
   
   static func parse(userInfo: [AnyHashable: Any]) -> Dispatch {
      guard let dispatch = userInfo[self.dispatchKey] as? Dispatch else {
         fatalError()
      }
      
      return dispatch
   }
   
   init(payload: Dispatch) {
      userInfo = [RollToDispatchNotification.dispatchKey: payload]
   }
}

struct DispatchSelectedNotification: AppNotification {
   private static let dispatchKey: String = "dispatch"
   
   var userInfo: [AnyHashable: Any]
   
   static func parse(userInfo: [AnyHashable: Any]) -> Dispatch {
      guard let dispatch = userInfo[self.dispatchKey] as? Dispatch else { fatalError() }
      
      return dispatch
   }
   
   init(payload dispatch: Dispatch) {
      userInfo = [DispatchSelectedNotification.dispatchKey: dispatch]
   }
}

struct LeftPanelInteractedNotification: AppNotification {
   private static let panelStateKey: String = "panelState"
   
   var userInfo: [AnyHashable: Any]
   
   static func parse(userInfo: [AnyHashable: Any]) -> PanelState {
      guard let state = userInfo[self.panelStateKey] as? PanelState else { fatalError() }
      
      return state
   }
   
   init(payload panelState: PanelState) {
      userInfo = [LeftPanelInteractedNotification.panelStateKey: panelState]
   }
}

struct PanelState {
   let isOpen: Bool
}

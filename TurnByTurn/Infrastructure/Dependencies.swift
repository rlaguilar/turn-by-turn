//
//  Dependencies.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation

class Dependencies {
   let dataManager: DataManagerType
   let accountManager: AccountManagerType
   let apiService: ApiServiceType
   let messagesHandler: MessagesHandler
   let keyboardHelper: KeyboardHelper
   
   init(dataManager: DataManagerType,
        accountManager: AccountManagerType,
        apiService: ApiServiceType,
        messagesHandler: MessagesHandler,
        keyboardHelper: KeyboardHelper) {
      
      self.dataManager = dataManager
      self.accountManager = accountManager
      self.apiService = apiService
      self.messagesHandler = messagesHandler
      self.keyboardHelper = keyboardHelper
   }
}

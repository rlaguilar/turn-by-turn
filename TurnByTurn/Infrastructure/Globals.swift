//
//  Globals.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import SwiftyBeaver

let log: SwiftyBeaver.Type = {
   let log = SwiftyBeaver.self
   let console = ConsoleDestination()
   //   console.minLevel = .info
   log.addDestination(console)
   return log
}()

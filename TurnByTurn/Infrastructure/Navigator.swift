//
//  Navigator.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import UIKit

import ArcGIS

class Navigator {
   enum Segue {
      case appMenu
      case dispatchDetails(for: Dispatch)
      case dispatchList(withFetcher: DispatchFetcher)
      case home
      case login
      case map(type: AGSBasemapType, usingFetcher: DispatchFetcher)
      case web(url: URL)
   }
   
   let dependencies: Dependencies
   
   init(dependencies: Dependencies) {
      self.dependencies = dependencies
   }
   
   func show(segue: Segue, transition: SceneInTransition, embeeded: Bool = false) {
      var vc: UIViewController
      
      defer {
         if embeeded {
            vc = UINavigationController(rootViewController: vc)
         }
         
         self.show(target: vc, transition: transition)
      }
      
      switch segue {
      case .appMenu:
         vc = MenuViewController.create(navigator: self, dependencies: dependencies)
         
      case .dispatchDetails(let dispatch):
         let viewModel = DispatchDetailsViewModel(
            dispatch: dispatch,
            apiService: dependencies.apiService
         )
         vc = DispatchDetailsViewController.create(
            viewModel: viewModel,
            navigator: self,
            dependencies: dependencies
         )
      case .dispatchList(let dispatchFetcher):
         let viewModel = DispatchListViewModel(dispatchFetcher: dispatchFetcher)
         vc = DispatchListViewController.create(
            viewModel: viewModel,
            navigator: self,
            dependencies: dependencies
         )
      case .home:
         let viewModel = HomeViewModel()
         vc = HomeViewController(
            viewModel: viewModel,
            navigator: self,
            dependencies: dependencies
         )
      case .login:
         let viewModel = LoginViewModel(apiService: dependencies.apiService)
         vc = LoginViewController.create(
            viewModel: viewModel,
            navigator: self,
            dependencies: dependencies
         )
      case .map(let baseMapType, let dispatchFetcher):
         vc = MapViewController.create(
            baseMapType: baseMapType,
            fetcher: dispatchFetcher,
            navigator: self,
            dependencies: dependencies
         )
      case .web(url: let url):
         vc = WebViewController.create(url: url, navigator: self, dependencies: dependencies)
      default:
         fatalError()
      }
   }
   
   private func show(target: UIViewController, transition: SceneInTransition) {
      switch transition {
      case .automatic(sender: let vc):
         vc.show(target, sender: vc)
      case .custom(transition: let block):
         block(target)
      case .modal(sender: let vc):
         vc.present(target, animated: true, completion: nil)
      case .push(sender: let vc):
         vc.pushViewController(target, animated: true)
      case .root(window: let window):
         window.rootViewController = target
      }
   }
}

enum SceneInTransition {
   case automatic(sender: UIViewController)
   case custom(transition: (UIViewController) -> Void)
   case modal(sender: UIViewController)
   case push(sender: UINavigationController)
   case root(window: UIWindow)
}

enum SceneOutTransition {
   case automatic(sender: UIViewController)
   case dismiss(sender: UIViewController)
   case pop(sender: UINavigationController)
   case navigationRoot(sender: UINavigationController)
}

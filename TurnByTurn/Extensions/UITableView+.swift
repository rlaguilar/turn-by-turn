//
//  UITableView+.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/28/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
   func register<CellType: UITableViewCell>(cellType type: CellType.Type, identifier: String) {
      let nibName = String(describing: type)
      let nib = UINib(nibName: nibName, bundle: .main)
      self.register(nib, forCellReuseIdentifier: identifier)
   }
   
   func register<CellType: UITableViewCell>(cellType type: CellType.Type) {
      let identifier = String(describing: type)
      self.register(cellType: type, identifier: identifier)
   }
   
   func dequeue<CellType: UITableViewCell>(indexPath: IndexPath, identifier: String) -> CellType {
      let rawCell = self.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
      
      guard let cell = rawCell as? CellType else {
         fatalError()
      }
      
      return cell
   }
   
   func dequeue<CellType: UITableViewCell>(indexPath: IndexPath) -> CellType {
      let identifier = String(describing: CellType.self)
      return self.dequeue(indexPath: indexPath, identifier: identifier)
   }
}

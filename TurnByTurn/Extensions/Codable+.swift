//
//  Codable+.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation

extension Decodable {
   static func fromJson(data: Data) throws -> Self {
      let decoder = JSONDecoder()
      decoder.dateDecodingStrategy = .custom({ (decoder) -> Date in
         let container = try decoder.singleValueContainer()
         let dateStr = try container.decode(String.self)
         
         guard let date = Date(universalStr: dateStr) else {
            throw AppError.message("Unable to parse date")
         }
         
         return date
      })
      return try decoder.decode(self, from: data)
   }
}

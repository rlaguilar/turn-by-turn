//
//  NSObject+.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import RxSwift

private struct AssociatedKeys {
   static var DisposeBagKey = "r_DisposeBagKey"
}

extension NSObject {
   /// Use this bag to dispose disposables upon the deallocation of the receiver.
   public var disposeBag: DisposeBag {
      if let disposeBag = objc_getAssociatedObject(self, &AssociatedKeys.DisposeBagKey) {
         return disposeBag as! DisposeBag
      } else {
         let disposeBag = DisposeBag()
         objc_setAssociatedObject(self, &AssociatedKeys.DisposeBagKey, disposeBag, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
         return disposeBag
      }
   }
}

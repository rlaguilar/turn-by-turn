//
//  Rx+.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

extension Variable where Element == String {
   @discardableResult
   func bind(label: UILabel) -> Disposable {
      let disposable = self.asObservable().bind(to: label.rx.text)
      disposable.disposed(by: label.disposeBag)
      return disposable
   }
}

infix operator <->

@discardableResult
func <-> (property: ControlProperty<String?>, variable: Variable<String>) -> Disposable {
   let bindToUIDisposable = variable.asObservable()
      .bind(to: property)
   
   let bindToVariable = property
      .subscribe(onNext: { n in
         variable.value = n ?? ""
      }, onCompleted:  {
         bindToUIDisposable.dispose()
      })
   
   return CompositeDisposable(bindToUIDisposable, bindToVariable)
}

@discardableResult
func <-> (textField: UITextField, variable: Variable<String>) -> Disposable {
   let disposable = (textField.rx.text <-> variable)
   disposable.disposed(by: textField.disposeBag)
   return disposable
}

@discardableResult
func <-> (textView: UITextView, variable: Variable<String>) -> Disposable {
   let disposable = textView.rx.text <-> variable
   disposable.disposed(by: textView.disposeBag)
   return disposable
}

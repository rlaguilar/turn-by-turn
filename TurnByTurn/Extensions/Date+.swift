//
//  Date+.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation

extension Date {
   static var format: String {
      return "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
   }
   
   func formatToUniversal() -> String {
      let formatter = DateFormatter()
      //""yyyy-MM-dd'T'HH:mm:ssxxx""
      formatter.dateFormat = Date.format
      return formatter.string(from: self)
   }
   
   init?(universalStr str: String) {
      let formatter = DateFormatter()
      formatter.dateFormat = Date.format
      
      if let date = formatter.date(from: str) {
         self.init(timeInterval: 0, since: date)
      }
      else {
         return nil
      }
   }
   
   func formatToReadable() -> String {
      let formatter = DateFormatter()
      formatter.dateFormat = "HH:mm:ss MMM, dd"
      return formatter.string(from: self)
   }
}

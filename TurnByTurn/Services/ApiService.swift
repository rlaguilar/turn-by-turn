//
//  ApiService.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

protocol ApiServiceType {
   func login(email: String, password: String) -> Observable<UserCredentials>
   
   func stopNotifications(for appToken: String) -> Observable<Void>
   
   func getDispatches(afterId: Int?, beforeId: Int?, count: Int) -> Observable<[Dispatch]>
   
   func getStory(forAddress id: Int) -> Observable<SizeupStory>
}

class ApiService: ApiServiceType {
   let client: NetworkClient
   
   private let userAuthHelper: UserAuthHelper
   private let storeAccessTokenHelper: StoreAccessTokenHelper
   private let tokenValidationHelper: TokenValidationHelper
   
   lazy var authedHelper: RequestHelper = {
      return CompoundHelper(helpers: self.userAuthHelper, self.tokenValidationHelper)
   }()
   
   init(client: NetworkClient,
        userAuthHelper: UserAuthHelper,
        storeAccessTokenHelper: StoreAccessTokenHelper,
        tokenValidationHelper: TokenValidationHelper
        ) {
      self.client = client
      self.userAuthHelper = userAuthHelper
      self.storeAccessTokenHelper = storeAccessTokenHelper
      self.tokenValidationHelper = tokenValidationHelper
   }
   
   func login(email: String, password: String) -> Observable<UserCredentials> {
      let resource = Resource(
         route: .login,
         method: .post,
         params: [
            "email": email,
            "password": password,
            "grant_type": "client_credentials"
         ],
         encoding: JSONEncoding.default,
         parser: UserCredentials.fromJson
      )
      
      return self.client.send(
         resource: resource,
         requestHelper: storeAccessTokenHelper
      )
   }
   
   func stopNotifications(for appToken: String) -> Observable<Void> {
      let resource = Resource(
         route: .removeToken,
         method: .delete,
         params: ["app_token": appToken, "service": "apns"],
         encoding: JSONEncoding.default) { _ in }
      
      return self.client.send(resource: resource, requestHelper: authedHelper)
   }
   
   func getDispatches(afterId: Int?, beforeId: Int?, count: Int) -> Observable<[Dispatch]> {
      let sinceId = afterId?.advanced(by: 1)
      let maxId = beforeId?.advanced(by: -1)
      
      var params: [String: Any] = ["page_size": count]
      
      if let since = sinceId {
         params["since_id"] = since
      }
      
      if let maxId = maxId {
         params["max_id"] = maxId
      }
      
      let resource = Resource(route: .getAlerts, params: params, parser: [Dispatch].fromJson)
      
      return client.send(resource: resource, requestHelper: authedHelper)
   }
   
   func getStory(forAddress id: Int) -> Observable<SizeupStory> {
      let resource = Resource(route: .story(alertId: id), parser: SizeupStory.fromJson)
      return self.client.send(resource: resource, requestHelper: authedHelper)
   }
}

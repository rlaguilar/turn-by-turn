//
//  DataManager.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation

protocol DataManagerType {
   var credentials: UserCredentials? { get set }
   
   var lastReadDispatch: Dispatch? { get set }
}

class DefaultDataManager: DataManagerType {
   let defaults = UserDefaults.standard
   let encoder = PropertyListEncoder()
   let decoder = PropertyListDecoder()
   
   var credentials: UserCredentials? {
      get {
         if let data = defaults.value(forKey: #function) as? Data {
            return try? decoder.decode(UserCredentials.self, from: data)
         }
         else {
            return nil
         }
      }
      
      set {
         let data = try? encoder.encode(newValue)
         defaults.set(data, forKey: #function)
      }
   }
   
   var lastReadDispatch: Dispatch? {
      get {
         if let data = defaults.value(forKey: #function) as? Data {
            return try? decoder.decode(Dispatch.self, from: data)
         }
         else { return nil }
      }
      
      set {
         let data = try? encoder.encode(newValue)
         defaults.set(data, forKey: #function)
      }
   }
}

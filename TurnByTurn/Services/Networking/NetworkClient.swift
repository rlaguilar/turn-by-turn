//
//  NetworkClient.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

class NetworkClient {
   lazy var manager: SessionManager = {
      // Create the server trust policies
      let serverTrustPolicies: [String: ServerTrustPolicy] = [
         "10.10.10.199": .disableEvaluation
      ]
      
      // Create custom manager
      let configuration = URLSessionConfiguration.default
      configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
      let manager = Alamofire.SessionManager(
         configuration: URLSessionConfiguration.default,
         serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
      )
      
      return manager
   }()
   
   let helper: RequestHelper
   
   init(helper: RequestHelper) {
      self.helper = helper
   }
   
   func send<T> (
      resource: Resource<T>,
      requestHelper: RequestHelper = EmptyHelper(),
      completion: @escaping (T?, Error?) -> Void) -> DataRequest {
      
      let compoundHelper = CompoundHelper(helpers: self.helper, requestHelper)
      
      compoundHelper.beforeSend()
      
      let task = manager.request(
         resource.route,
         method: resource.method,
         parameters: resource.params,
         encoding: resource.encoding,
         headers: compoundHelper.headers
         )
         .validate(statusCode: (100 ..< 500))
         .responseData { (response) in
            switch response.result {
            case .failure(let error):
               compoundHelper.afterError(error: error, code: response.response?.statusCode)
               completion(nil, error)
            case .success(let data):
               do {
                  let result = try resource.parse(response: data)
                  compoundHelper.afterSuccess(value: result)
                  completion(result, nil)
               }
               catch {
                  compoundHelper.afterError(error: error, code: response.response?.statusCode)
                  completion(nil, error)
               }
            }
      }
      
      
      print(task.debugDescription)
      
      task.resume()
      
      return task
   }
   
   func send<T> (
      resource: Resource<T>,
      requestHelper: RequestHelper = EmptyHelper()) -> Observable<T> {
      
      return Observable.create({ (observer) -> Disposable in
         let task = self.send(resource: resource, requestHelper: requestHelper, completion: { (value, error) in
            
            if let error = error {
               observer.onError(error)
            }
            else {
               guard let value = value else {
                  fatalError()
               }
               
               observer.onNext(value)
               observer.onCompleted()
            }
         })
         
         return Disposables.create {
            task.cancel()
         }
      })
   }
}

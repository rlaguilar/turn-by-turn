//
//  Resource.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import Alamofire
import Mapper

struct Resource<T> {
   let method: HTTPMethod
   let route: Route
   let params: [String: Any]
   let encoding: ParameterEncoding
   private let parser: (Data) throws -> T
   
   init(route: Route,
        method: HTTPMethod = .get,
        params: [String: Any] = [:],
        encoding: ParameterEncoding = URLEncoding.default,
        parser: @escaping (Data) throws -> T
      ) {
      self.route = route
      self.method = method
      self.params = params
      self.encoding = encoding
      self.parser = parser
   }
   
   func parse(response data: Data) throws -> T {
      let decoder = JSONDecoder()
      let failedResponse: FailedResponse
      
      do {
         failedResponse = try decoder.decode(FailedResponse.self, from: data)
      }
      catch {
         return try parser(data)
      }
      
      throw AppError.message(failedResponse.message)
   }
}

//
//  SharedRequestHelpers.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation

class UserAuthHelper: RequestHelper {
   let dataManager: DataManagerType
   
   var headers: [String : String] {
      let token = dataManager.credentials?.authToken ?? ""
      return ["Authorization": "Bearer \(token)"]
   }
   
   init (dataManager: DataManagerType) {
      self.dataManager = dataManager
   }
}

class StoreAccessTokenHelper: RequestHelper {
   var accountManager: AccountManagerType
   
   init(accountManager: AccountManagerType) {
      self.accountManager = accountManager
   }
   
   func afterSuccess(value: Any) {
      guard let credentials = value as? UserCredentials else { return }
      
      self.accountManager.authStatus = .autorized(credentials)
   }
   
}

class TokenValidationHelper: RequestHelper {
   var accountManager: AccountManagerType
   
   init(accountManager: AccountManagerType) {
      self.accountManager = accountManager
   }
   
   func afterError(error: Error, code: Int?) {
      if code == 401 {
         accountManager.authStatus = .unauthorized
      }
   }
}

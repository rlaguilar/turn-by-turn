//
//  SearchResultsViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/28/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit

import RxSwift
import ArcGIS

class SearchResultsViewController: UITableViewController {
   private let _selectedItem = PublishSubject<SearchResult>()
   lazy var selectedItem = self._selectedItem.asObservable()
   
   var items: [SearchResult] = [] {
      didSet {
         self.tableView.reloadData()
      }
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.tableView.register(cellType: SearchResultCell.self)
      
      self.tableView.rx.itemSelected
         .map { [unowned self] index in self.items[index.row] }
         .bind(to: self._selectedItem)
         .disposed(by: disposeBag)
   }
   
   // MARK: - Table view data source
   
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      // #warning Incomplete implementation, return the number of rows
      return items.count
   }
   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell: SearchResultCell = tableView.dequeue(indexPath: indexPath)
      cell.searchResultLabel.text = items[indexPath.row].name
      return cell
   }
   
   /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
   
   /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
    // Delete the row from the data source
    tableView.deleteRows(at: [indexPath], with: .fade)
    } else if editingStyle == .insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
   
   /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
    
    }
    */
   
   /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
   
   /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
   
}

struct SearchResult {
   let name: String
   let position: AGSPoint?
}

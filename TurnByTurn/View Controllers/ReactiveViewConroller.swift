//
//  ReactiveViewConroller.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import UIKit

import RxCocoa
import RxSwift

class ReactiveViewController: UIViewController {
   private let _whenViewAppeared = ReplaySubject<Void>.create(bufferSize: 1)
   lazy var whenViewAppeared = self._whenViewAppeared.asObservable()
   
   private let layedOut = ReplaySubject<Void>.create(bufferSize: 1)
   lazy var whenLayedOut = self.layedOut.asObservable()
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      layedOut.onNext(())
   }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      _whenViewAppeared.onNext(())
   }
}

class ReactiveCollectionViewController: UICollectionViewController {
   private let layedOut = ReplaySubject<Void>.create(bufferSize: 1)
   lazy var whenLayedOut = self.layedOut.asObservable()
   
   private let didAppear = ReplaySubject<Void>.create(bufferSize: 1)
   lazy var whenDidAppear = self.didAppear.asObservable()
   
   private let didDisappear = ReplaySubject<Void>.create(bufferSize: 1)
   lazy var whenDidDisappear = self.didDisappear.asObservable()
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      layedOut.onNext(())
   }
   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      didAppear.onNext(())
   }
   
   override func viewDidDisappear(_ animated: Bool) {
      super.viewDidDisappear(animated)
      didDisappear.onNext(())
   }
}

//
//  MapViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit
import ArcGIS
import RxCocoa
import RxSwift

class MapViewController: UIViewController {
   static func create(
      baseMapType: AGSBasemapType,
      fetcher: DispatchFetcher,
      navigator: Navigator,
      dependencies: Dependencies) -> MapViewController {

      let vc = self.instantiate(from: .main)
      vc.navigator = navigator
      vc.dependencies = dependencies
      vc.baseMapType = baseMapType
      vc.dispatchFetcher = fetcher
      
      return vc
   }
   
   var navigator: Navigator!
   var dependencies: Dependencies!
   var baseMapType: AGSBasemapType!
   var dispatchFetcher: DispatchFetcher!
   
   @IBOutlet weak var mapView: AGSMapView!
   @IBOutlet weak var searchBarWrapper: UIView!
   
   private var graphicsOverlay = AGSGraphicsOverlay()
   
   var dispatches: [AGSPoint: Dispatch] = [:]
   
   var searchController: SearchController!
   var resultsController: SearchResultsViewController!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      mapView.map = AGSMap(basemapType: baseMapType, latitude: 34.057, longitude: -117.196, levelOfDetail: 10)
      
      mapView.graphicsOverlays.add(graphicsOverlay)
      
      mapView.touchDelegate = self
      
      resultsController = SearchResultsViewController()
      searchController = SearchController(searchResultsController: resultsController)
      
      let searchBar = searchController.searchBar
      searchBar.sizeToFit()
      searchBarWrapper.addSubview(searchBar)
      searchBar.frame = searchBarWrapper.bounds
      searchBar.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
      
      self.configureSearch()
      
      self.addLayers()
      
      dispatchFetcher.timeline
         .observeOn(MainScheduler.instance)
         .subscribe(onNext: { [unowned self] newItems in
            let image = #imageLiteral(resourceName: "ic_incident")
            let symbol = AGSPictureMarkerSymbol(image: image)
            symbol.offsetY = image.size.height / 2
            
            let pairs = newItems.filter { $0.coordinates != nil }
               .map { ($0, AGSPoint(clLocationCoordinate2D: $0.coordinates!)) }
            
            pairs.forEach { [unowned self] in self.dispatches[$1] = $0 }

            let multipoints = AGSMultipoint(points: pairs.map { $1 })
            let graphic = AGSGraphic(geometry: multipoints, symbol: symbol, attributes: nil)
            self.graphicsOverlay.graphics.add(graphic)
      }).disposed(by: disposeBag)
      
      DispatchSelectedNotification.observe.subscribe(onNext: { [unowned self] dispatch in
         self.mapView.callout.dismiss()
         
         if let coordinates = dispatch.coordinates {
            let point = AGSPoint(clLocationCoordinate2D: coordinates)
            self.mapView.setViewpointCenter(point, scale: 4500, completion: { _ in
               log.info("Finished moving the map")
            })
         }
      }).disposed(by: disposeBag)
      
      mapView.locationDisplay.dataSource = AGSCLLocationDataSource()
      mapView.locationDisplay.autoPanMode = .recenter
      
      mapView.locationDisplay.start { (error) in
         if let error = error {
            log.error("Unable to start displaying the location: \(error)")
         }
         else {
            log.info("Successfully displayed user location")
         }
      }
   }
   
   func configureSearch() {
      let locatorStr = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer"
      
      guard let locatorUrl = URL(string: locatorStr) else { return }
      
      let locatorTask = AGSLocatorTask(url: locatorUrl)
      
      let params = AGSGeocodeParameters()
      params.resultAttributeNames.append("*")
      params.minScore = 90
      
      self.searchController.updateResults.flatMapLatest { [weak self] (query) -> Observable<[AGSGeocodeResult]>
         in
         guard let `self` = self,
               let location = self.mapView.locationDisplay.location?.position?.toCLLocationCoordinate2D()
            else { return .empty() }
         
         params.preferredSearchLocation = AGSPoint(clLocationCoordinate2D: location)
         
         return Observable.create({ (observer) -> Disposable in
            let cancelable = locatorTask.geocode(
                  withSearchText: query,
                  parameters: params) { (result, error) in
               
               guard let result = result else {
                  observer.onCompleted()
                  return
               }
               
               observer.onNext(result)
               observer.onCompleted()
            }
            
            return Disposables.create {
               cancelable.cancel()
            }
         })
         }.map { (items) in items.map({ item in
               SearchResult(
                  name: item.label,
                  position: item.displayLocation
               )
            })
         }
         .subscribe(onNext: { [unowned self] items in
            self.resultsController.items = items
         }).disposed(by: disposeBag)
      
      self.resultsController.selectedItem.subscribe(onNext: { [unowned self] result in
         self.searchController.isActive = false
         guard let position = result.position else { return }
         self.mapView.setViewpointCenter(position, scale: 4500, completion: nil)
      }).disposed(by: disposeBag)
   }
   
   func addLayers() {
      addLayer(url: "https://services8.arcgis.com/jNUIWEZv9aaHqUtJ/ArcGIS/rest/services/FirstDueSizeUp_Preplan_Data/FeatureServer/0")
      addLayer(url: "https://services8.arcgis.com/jNUIWEZv9aaHqUtJ/ArcGIS/rest/services/FirstDueSizeUp_Hydrants/FeatureServer/0")
   }
   
   func addLayer(url: String) {
      guard let url = URL(string: url) else {
         fatalError()
      }
      
      let featureTable = AGSServiceFeatureTable(url: url)
      let layer = AGSFeatureLayer(featureTable: featureTable)
      
      self.mapView.map?.operationalLayers.add(layer)
   }
   
   lazy var calloutView: DispatchCalloutView = {
      guard let view = UINib(
         nibName: "DispatchCalloutView",
         bundle: .main).instantiate(withOwner: nil, options: nil)[0] as? DispatchCalloutView else {
            fatalError()
      }
      
      return view
   }()
}

extension MapViewController: AGSGeoViewTouchDelegate {
   func geoView(_ geoView: AGSGeoView, didTapAtScreenPoint screenPoint: CGPoint, mapPoint: AGSPoint) {
      let mapPoint = AGSPoint(clLocationCoordinate2D: mapPoint.toCLLocationCoordinate2D())
   
      mapView.callout.dismiss()
      
      self.mapView.identify(self.graphicsOverlay, screenPoint: screenPoint, tolerance: 12, returnPopupsOnly: false) { [unowned self] (result) in
         if let geometry = result.graphics.first?.geometry as? AGSMultipoint {
            if let point = geometry.points.array()
               .min(by: { mapPoint.distance(to: $0) < mapPoint.distance(to: $1) }) {
             
               if let dispatch = self.dispatches[point] {
                  self.showCallout(for: dispatch)
               }
            }
         }
      }
   }
   
   func showCallout(for dispatch: Dispatch) {
      guard let coordinates = dispatch.coordinates else {
         fatalError()
      }
      
      let point = AGSPoint(clLocationCoordinate2D: coordinates)
      let calloutView = self.calloutView
      calloutView.update(item: dispatch, api: self.dependencies.apiService)
      
      calloutView.sizeToFit()
      mapView.callout.customView = calloutView
      
      mapView.callout.show(
         at: point,
         screenOffset: CGPoint(x: 0, y: -46),
         rotateOffsetWithMap: false,
         animated: true
      )
   }
}

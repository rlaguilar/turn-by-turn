//
//  WebViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/10/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import RxCocoa
import RxSwift

class WebViewController: UIViewController, UIWebViewDelegate, NVActivityIndicatorViewable {
   @IBOutlet weak var webView: UIWebView!
   
   static func create(url: URL, navigator: Navigator, dependencies: Dependencies) -> WebViewController {
      
      let vc = WebViewController.instantiate(from: .main)
      vc.url = url
      vc.navigator = navigator
      vc.dependencies = dependencies
      return vc
   }
   
   var url: URL!
   var dependencies: Dependencies!
   var navigator: Navigator!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.webView.delegate = self
      // Do any additional setup after loading the view.
      
      let loading = webView.rx.didStartLoad.take(1).do(onNext: { [unowned self] in
         self.startAnimating(
            CGSize(width: 200, height: 200),
            message: "Loading page content",
            type: .ballScaleRippleMultiple,
            color: UIColor(white: CGFloat(51) / 255, alpha: 1),
            padding: 40,
            backgroundColor: .clear
         )
      })
   
      let finished = Observable.from([
         webView.rx.didFinishLoad.map { _ -> AppError? in nil },
         webView.rx.didFailLoad.map { error -> AppError? in
            if (error as NSError).code == -999 {
               return nil
            }
            else {
               return AppError(error: error)
            }
         }
         ]).merge()
      
      Observable.combineLatest(loading, finished) { (_, failed)  in failed }
         .do(onNext: { [unowned self] _ in
            self.stopAnimating()
         })
         .filter { $0 != nil }
         .subscribe(onNext: { [unowned self] _ in
            self.dependencies.messagesHandler.showError(
               message: "Unable to load the web page. Please, try to reopen it"
            )
         }).disposed(by: disposeBag)
      
      if let url = url {
         self.loadUrl(url: url)
      }
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      self.navigationController?.setNavigationBarHidden(false, animated: true)
   }
   
   func loadUrl(url: URL) {
      let request = URLRequest(url: url)
      
      let authedRequest = modify(
         request: request,
         token: self.dependencies.dataManager.credentials?.authToken ?? ""
      )
      
      log.info(authedRequest)
      webView.loadRequest(authedRequest)
   }
   
   func modify(request: URLRequest, token: String) -> URLRequest {
      var request = request
      request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
      return request
   }
   
   func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
      log.debug("Failed loading request")
      log.error(error)
   }
}

//
//  LoginViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import UIKit

import RxSwift
import NVActivityIndicatorView

class LoginViewController: ReactiveViewController {
   static func create(
      viewModel: LoginViewModel,
      navigator: Navigator,
      dependencies: Dependencies) -> LoginViewController {
      
      let vc = LoginViewController.instantiate(from: .main)
      vc.viewModel = viewModel
      vc.navigator = navigator
      vc.dependencies = dependencies
      return vc
   }
   
   @IBOutlet weak var logoView: UIImageView!
   @IBOutlet weak var welcomeLabel: UILabel!
   
   @IBOutlet weak var signInButton: UIButton!
   
   @IBOutlet weak var dimBackgroundView: UIView!
   
   @IBOutlet weak var loginFieldsContainer: UIView!
   @IBOutlet weak var emailField: UITextField!
   @IBOutlet weak var passwordField: UITextField!
   @IBOutlet weak var loginButton: UIButton!
   @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
   
   @IBOutlet var viewsToHide: [UIView]!
   
   
   var viewModel: LoginViewModel!
   var navigator: Navigator!
   var dependencies: Dependencies!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.handleKeyboard()
      
      emailField <-> viewModel.username
      passwordField <-> viewModel.password
      loginButton.rx.bind(to: viewModel.login, input: ())
      
      loginButton.rx.tap.bind { [unowned self] in
         self.activityIndicatorView.startAnimating()
      }.disposed(by: disposeBag)
      
      viewModel.login.elements.subscribe(onNext: {[unowned self] credentials in
         self.activityIndicatorView.stopAnimating()
      }).disposed(by: disposeBag)
      
      viewModel.login.errors.subscribe(onNext: { [unowned self] actionError in
         self.activityIndicatorView.stopAnimating()
         
         switch actionError {
         case .underlyingError(let error):
            guard let appError = error as? AppError else {
               return
            }
            
            self.dependencies.messagesHandler.showError(message: appError.description)
         default:
            break
         }
      }).disposed(by: disposeBag)
      
      dimBackgroundView.alpha = 0
      
      let tap = UITapGestureRecognizer()
      dimBackgroundView.addGestureRecognizer(tap)
      
      Observable.of(
         tap.rx.event.filter { [unowned self] gesture in
            guard let view = self.loginFieldsContainer else {
               return true
            }
            
            let location = gesture.location(in: view)
            
            return !view.bounds.contains(location)
         }.map { _ in },
         signInButton.rx.tap.asObservable()
      ).merge()
      .bind {
         UIView.animate(withDuration: 0.3, animations: { [unowned self] in
            self.dimBackgroundView.alpha = self.dimBackgroundView.alpha == 0 ? 1 : 0
         })
      }.disposed(by: disposeBag)
      
      whenLayedOut.takeUntil(self.whenViewAppeared).subscribe(onNext: { [unowned self] in
         let externalCenter = self.view.center
         let center = self.logoView.superview!.convert(externalCenter, from: self.view)
         let size = CGSize(width: 200, height: 180)
         
         let scale = CGAffineTransform(
            scaleX: size.width / self.logoView.bounds.width,
            y: size.height / self.logoView.bounds.height
         )
         
         let translate = CGAffineTransform(
            translationX: center.x - self.logoView.center.x,
            y: center.y - self.logoView.center.y
         )
         
         let transform = scale.concatenating(translate)
         self.logoView.transform = transform
      }).disposed(by: disposeBag)
      
      viewsToHide.forEach { v in
         v.alpha = 0
         v.transform = CGAffineTransform(translationX: 0, y: -40)
      }
      
      whenViewAppeared.take(1).subscribe(onNext: {
         UIView.animate(withDuration: 1, delay: 0.3, animations: {
            self.logoView.transform = .identity
         }, completion: { (_) in
            UIView.animate(withDuration: 1.8, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: { [unowned self] in
               self.viewsToHide.forEach { v in
                  v.alpha = 1
                  v.transform = .identity
               }
            }, completion: nil)
         })
      }).disposed(by: disposeBag)
   }
   
   func handleKeyboard() {
      self.dependencies.keyboardHelper.currentKeyboardState.drive(onNext: { [unowned self] state in
         let height = state.intersectionHeight(with: self.loginFieldsContainer)
         
         if height > 0 {
            self.loginFieldsContainer.transform = CGAffineTransform(translationX: 0, y: -height - 20)
         }
         else {
            self.loginFieldsContainer.transform = .identity
         }
         
      }).disposed(by: disposeBag)
   }

}

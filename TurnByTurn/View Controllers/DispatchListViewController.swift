//
//  DispatchListViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class DispatchListViewController: ReactiveCollectionViewController {
   static func create(viewModel: DispatchListViewModel,
                      navigator: Navigator,
                      dependencies: Dependencies) -> DispatchListViewController {
      let vc = DispatchListViewController.instantiate(from: .main)
      vc.viewModel = viewModel
      vc.navigator = navigator
      vc.dependencies = dependencies
      vc.dataManager = dependencies.dataManager
      return vc
   }
   
   var viewModel: DispatchListViewModel!
   var navigator: Navigator!
   var dependencies: Dependencies!
   var dataManager: DataManagerType!
   
   var dispatches: [DispatchItemViewModel] = []
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      viewModel.latestDispatches.filter { !$0.isEmpty }
         .subscribe(onNext: { [unowned self] newItems in
            let newDispatches = newItems.map { [unowned self] item -> DispatchItemViewModel in
               let vm = DispatchItemViewModel(dispatch: item)
               
               if let dispatch = self.dataManager.lastReadDispatch {
                  vm.isRead.value = dispatch.id >= item.id
               }
               else {
                  vm.isRead.value = false
               }
               
               return vm
            }
            self.dispatches.insert(contentsOf: newDispatches, at: 0)
            let indexes = (0 ..< newItems.count).map { IndexPath(item: $0, section: 0) }
            self.collectionView?.insertItems(at: indexes)
         }
      ).disposed(by: disposeBag)
      
      self.whenLayedOut.take(1).subscribe(onNext: { [unowned self] in
         guard let collectionView = self.collectionView,
            let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
         
         var itemSize = layout.itemSize
         itemSize.width = collectionView.bounds.width
         layout.itemSize = itemSize
         layout.minimumLineSpacing = 4
      }).disposed(by: disposeBag)
      
      LeftPanelInteractedNotification.observe
         .map { $0.isOpen }
         .distinctUntilChanged().subscribe(onNext: { [unowned self] isOpen in
            if !isOpen {
               self.markAllAsRead()
            }
      }).disposed(by: disposeBag)
   }
   
   override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return dispatches.count
   }
   
   override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      guard let cell = collectionView.dequeueReusableCell(
         withReuseIdentifier: "identifier",
         for: indexPath
         ) as? DispatchCollectionViewCell else { fatalError() }
      
      let item = dispatches[indexPath.item]
      cell.item = item.dispatch
      
      let trigger = Observable.of(self.whenDidAppear, NotificationCenter.default.rx.notification(.UIApplicationDidBecomeActive).map { _ in },
                                  item.isRead.asObservable().map { _ in }
      ).merge()
      
      trigger.withLatestFrom(item.isRead.asObservable())
         .subscribe(onNext: { [unowned self] isRead in
            self.notifyReadingState(cell: cell, isRead: isRead)
         })
         .disposed(by: cell.reuseBag)
      
      return cell
   }
   
   func notifyReadingState(cell: DispatchCollectionViewCell, isRead: Bool) {
      if isRead {
         cell.layer.removeAllAnimations()
         cell.backgroundColor = .clear
      }
      else {
         cell.backgroundColor = #colorLiteral(red: 0.1529999971, green: 0.5249999762, blue: 0.753000021, alpha: 1)
         
         UIView.animate(
            withDuration: 0.3,
            delay: 0,
            options: [.allowUserInteraction, .autoreverse, .repeat],
            animations: {
               cell.backgroundColor = .black
            },
            completion: nil
         )
      }
   }
   
   override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      let item = dispatches[indexPath.item]
      markAsRead(item: item)
      DispatchSelectedNotification.post(payload: item.dispatch, sender: self)
      navigator.show(segue: .dispatchDetails(for: item.dispatch), transition: .automatic(sender: self))
   }
   
   private func markAllAsRead() {
      for item in self.dispatches {
         self.markAsRead(item: item)
      }
   }
   
   private func markAsRead(item: DispatchItemViewModel) {
      guard !item.isRead.value else { return }
      
      if let dispatch = dataManager.lastReadDispatch {
         if item.dispatch.id > dispatch.id {
            dataManager.lastReadDispatch = item.dispatch
         }
      }
      else {
         dataManager.lastReadDispatch = item.dispatch
      }
      
      item.isRead.value = true
   }
}

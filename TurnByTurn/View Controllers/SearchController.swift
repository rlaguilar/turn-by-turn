//
//  SearchController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/28/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit
import RxSwift

class SearchController: UISearchController, UISearchResultsUpdating {
   private let _update = PublishSubject<String>()
   lazy var updateResults = self._update.asObservable()
   
   func initialize() {
      self.searchResultsUpdater = self
      searchBar.barStyle = .black
      searchBar.searchBarStyle = .minimal
   }
   
   func updateSearchResults(for searchController: UISearchController) {
      _update.onNext(searchBar.text ?? "")
   }
   
   override init(searchResultsController: UIViewController?) {
      super.init(searchResultsController: searchResultsController)
      self.initialize()
   }
   
   override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
      super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
      self.initialize()
   }
   
   required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      self.initialize()
   }
}

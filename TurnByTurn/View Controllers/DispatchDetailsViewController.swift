//
//  DispatchDetailsViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/10/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit
import CoreLocation

import Action
import RxCocoa
import RxSwift

class DispatchDetailsViewController: ReactiveViewController {
   static func create(viewModel: DispatchDetailsViewModel, navigator: Navigator, dependencies: Dependencies) -> DispatchDetailsViewController {
      let vc = DispatchDetailsViewController.instantiate(from: .main)
      vc.viewModel = viewModel
      vc.navigator = navigator
      vc.dependencies = dependencies
      return vc
   }
   
   @IBOutlet weak var metadataContainer: UIStackView!
   @IBOutlet weak var showSizeupButton: UIButton!
   @IBOutlet weak var rollButton: UIButton!
   @IBOutlet weak var dispatchNumberView: UIStackView!
   @IBOutlet weak var dispatchNumberLabel: UILabel!
   @IBOutlet weak var dispatchAddressLabel: UILabel!
   @IBOutlet weak var dispatchCityLabel: UILabel!
   @IBOutlet weak var dispatchDateLabel: UILabel!
   
   @IBOutlet weak var statusButton: UIButton!
   
   @IBOutlet weak var incidentTypeView: UIStackView!
   @IBOutlet weak var incidentTypeLabel: UILabel!
   @IBOutlet weak var unitsView: UIStackView!
   @IBOutlet weak var unitsLabel: UILabel!
   @IBOutlet weak var callNotesView: UIStackView!
   @IBOutlet weak var callNotesLabel: UILabel!
   @IBOutlet weak var buildingStoryView: UIStackView!
   @IBOutlet weak var buildingStoryLabel: UILabel!
   
   var viewModel: DispatchDetailsViewModel!
   var navigator: Navigator!
   var dependencies: Dependencies!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      [rollButton, showSizeupButton].forEach { button in
         let layer = button?.layer
         layer?.shadowOpacity = 0.6
         layer?.shadowOffset = .zero
      }
      
      if #available(iOS 11, *) {
         self.title = "ID: \(viewModel.number.value)"
         self.dispatchNumberView.isHidden = true
         metadataContainer.insertArrangedSubview(dispatchDateLabel, at: 0)
      }
      else {
         viewModel.number.bind(label: dispatchNumberLabel)
      }
      
      if let url = viewModel.sizeupUrl {
         self.showSizeupButton.rx.tap.bind { [unowned self] in
            OpenUrlNotification.post(payload: url, sender: self)
         }.disposed(by: disposeBag)
      }
      else {
         self.showSizeupButton.isHidden = true
      }
      
      if viewModel.coordinates != nil {
         self.rollButton.rx.tap.bind { [unowned self] in
            RollToDispatchNotification.post(payload: self.viewModel.dispatch, sender: self)
         }.disposed(by: disposeBag)
      }
      else {
         self.rollButton.isHidden = true
      }
      
      viewModel.address.bind(label: dispatchAddressLabel)
      viewModel.city.bind(label: dispatchCityLabel)
      viewModel.created.bind(label: dispatchDateLabel)
      
      [
         (viewModel.dispatchType, incidentTypeLabel, incidentTypeView),
         (viewModel.units, unitsLabel, unitsView),
         (viewModel.callNotes, callNotesLabel, callNotesView),
         (viewModel.buildingStory, buildingStoryLabel, buildingStoryView)
      ].forEach { (variable, label, container) in
         variable.asObservable().subscribe(onNext: { value in
            container.isHidden = value == nil
            label.text = value
         }).disposed(by: disposeBag)
      }
   }
}

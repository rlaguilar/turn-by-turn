//
//  MenuViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/10/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit

private let identifier = "identifier"

class MenuViewController: UITableViewController {
   static func create(navigator: Navigator, dependencies: Dependencies) -> MenuViewController {
      let vc = MenuViewController()
      vc.navigator = navigator
      vc.dependencies = dependencies
      return vc
   }
   //   let items = ["Settings", "Notifications", "Pre-Plans", "Enhancement requests", "Dev Info", "Logout", "Driving Simulator"]
   //   let itemImages = [ #imageLiteral(resourceName: "ic_settings"), #imageLiteral(resourceName: "ic_notifications"), #imageLiteral(resourceName: "ic_preplan"), #imageLiteral(resourceName: "ic_new_features"), #imageLiteral(resourceName: "ic_dev_tools"), #imageLiteral(resourceName: "ic_logout"), LAFDStyleKit.imageOfDrivingSimulatorIcon ]
   
   var items: [MenuItem] = []
   var navigator: Navigator!
   var dependencies: Dependencies!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      self.title = "Menu Options"
      self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: identifier)
      self.tableView.tableFooterView = UIView()
      tableView.backgroundColor = #colorLiteral(red: 0.1840000004, green: 0.2199999988, blue: 0.2509999871, alpha: 1)
      tableView.rowHeight = 50
      
      items = [
         MenuItem(name: "Settings", image: #imageLiteral(resourceName: "ic_settings"), action: { [weak self] in
            self?.show(url: Route.settings.absoluteUrl)
         }),
         MenuItem(name: "Notifications", image: #imageLiteral(resourceName: "ic_notifications"), action: { [weak self] in
            self?.clearSelection()
         }),
         MenuItem(name: "Pre-Plans", image: #imageLiteral(resourceName: "ic_preplan"), action: { [weak self] in
            self?.show(url: Route.preplanWeb.absoluteUrl)
         }),
         MenuItem(name: "Enhancement requests", image: #imageLiteral(resourceName: "ic_new_features"), action: { [weak self] in
            self?.show(url: Route.commingSoonFeatures.absoluteUrl)
         }),
         MenuItem(name: "Logout", image: #imageLiteral(resourceName: "ic_logout"), action: { [weak self] in
            self?.logout()
         }),
      ]
   }
   
   func show(url: URL) {
      OpenUrlNotification.post(payload: url, sender: self)
   }
   
   func clearSelection() {
      if let index = self.tableView.indexPathForSelectedRow {
         self.tableView.deselectRow(at: index, animated: true)
      }
   }
   
   func logout() {
      self.dependencies.accountManager.logout().subscribe(onNext: {
         log.info("LoggedOut")
      }).disposed(by: disposeBag)
   }
   
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return items.count
   }
   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(
         withIdentifier: identifier,
         for: indexPath)
      
      let item = self.items[indexPath.row]
      cell.textLabel?.text = item.name
      cell.imageView?.image = item.image
      cell.tintColor = .white
      cell.textLabel?.textColor = .white
      cell.backgroundColor = .clear
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let item = self.items[indexPath.row]
      item.action()
   }
   
   /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */
   
   /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
    // Delete the row from the data source
    tableView.deleteRows(at: [indexPath], with: .fade)
    } else if editingStyle == .insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
   
   /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
    
    }
    */
   
   /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
   
   /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
   struct MenuItem {
      let name: String
      let image: UIImage
      let action: () -> Void
   }
}

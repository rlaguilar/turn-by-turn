//
//  HomeViewController.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit
import RxSwift

class HomeViewController: SplitViewController {
   func create(viewModel: HomeViewModel,
               navigator: Navigator,
               dependecies: Dependencies) -> HomeViewController {
      
      return HomeViewController(viewModel: viewModel, navigator: navigator, dependencies: dependecies)
   }
   
   let viewModel: HomeViewModel
   let navigator: Navigator
   let dependencies: Dependencies
   let dispatchFetcher: DispatchFetcher
   
   init(viewModel: HomeViewModel, navigator: Navigator, dependencies: Dependencies) {
      self.viewModel = viewModel
      self.navigator = navigator
      self.dependencies = dependencies
      
      self.dispatchFetcher = DispatchFetcher(
         apiService: dependencies.apiService,
         pageSize: 20
      )
      
      let leftPanel = UINavigationController()
      navigator.show(segue: .dispatchList(withFetcher: dispatchFetcher), transition: .push(sender: leftPanel))
      
      let middlePanel = UINavigationController()
      navigator.show(
         segue: .map(type: .navigationVector, usingFetcher: dispatchFetcher),
         transition: .push(sender: middlePanel)
      )
      
      let rightPanel = UINavigationController()
      navigator.show(segue: .appMenu, transition: .push(sender: rightPanel))
      
      if #available(iOS 11.0, *) {
         leftPanel.navigationBar.prefersLargeTitles = true
         rightPanel.navigationBar.prefersLargeTitles = true
      } else {
         // Fallback on earlier versions
         rightPanel.isNavigationBarHidden = true
      }
      
      middlePanel.isNavigationBarHidden = true
      
      super.init(leftVC: leftPanel, middleVC: middlePanel, rightVC: rightPanel)
   }
   
   required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
   
      // Do any additional setup after loading the view.
      self.navigationController?.isNavigationBarHidden = true
      let leftButtonColor = #colorLiteral(red: 0.175999999, green: 0.7329999804, blue: 0.9060000181, alpha: 1)
      self.leftToggleButton.backgroundColor = leftButtonColor
      self.rightToggleButton.backgroundColor = #colorLiteral(red: 0.07800000161, green: 0.3759999871, blue: 0.3919999897, alpha: 1)
      
      // TODO: Refactor code to make it more legible
      
      // handling notifications button animation
      let trigger: Observable<Void> = Observable.of(
         dispatchFetcher.timeline.filter { !$0.isEmpty }.map { _ in },
         NotificationCenter.default.rx.notification(.UIApplicationDidBecomeActive).map { _ in },
         self.whenViewAppeared,
         self.isLeftOpened.asObservable().map { _ in }
      ).merge()
      
      let dataManager = dependencies.dataManager
      
      let unreadItems = Observable.of(
         dispatchFetcher.timeline.filter { dispatches in // filter unread items
            guard let mostRecent = dispatches.first else { return false }
            
            if mostRecent.id > (dataManager.lastReadDispatch?.id ?? 0) {
               return true
            }
            else {
               return false
            }
         }.withLatestFrom(self.isLeftOpened.asObservable().map { !$0 }),
         self.isLeftOpened.asObservable().filter { $0 }.map { _ in false }
      ).merge()
      .startWith(false)
      
      trigger.withLatestFrom(unreadItems).subscribe(onNext: { unread in
         if unread {
            UIView.animate(
               withDuration: 0.2,
               delay: 0,
               options: [.autoreverse, .repeat, .allowUserInteraction],
               animations: { [unowned self]  in
                  self.leftToggleButton.backgroundColor = .red
            }, completion: nil)
         }
         else {
            self.leftToggleButton.layer.removeAllAnimations()
            self.leftToggleButton.backgroundColor = leftButtonColor
         }
      }).disposed(by: disposeBag)
      
      // notify left panel handing
      self.isLeftOpened.asObservable().distinctUntilChanged()
         .subscribe(onNext: { [weak self] isOpen in
            LeftPanelInteractedNotification.post(
               payload: PanelState(isOpen: isOpen),
               sender: self
            )
         }).disposed(by: disposeBag)
      
      // handling url opening
      OpenUrlNotification.observe.subscribe(onNext: { [unowned self] url in
         self.navigator.show(
            segue: .web(url: url),
            transition: .automatic(sender: self)
         )
      }).disposed(by: disposeBag)
      
      // handling roll to dispatch
      RollToDispatchNotification.observe.filter { $0.coordinates != nil }
         .subscribe(onNext: { [weak self] dispatch in
            guard let coordinates = dispatch.coordinates else { fatalError() }
            
            let stop = "\(coordinates.latitude),\(coordinates.longitude)"
            var urlStr = "arcgis-navigator://?stop=\(stop)&travelmode=Trucking+Time"

            if let address = dispatch.address {
               urlStr += "&stopname=\(address)"
            }
            
//            let stop = "41.833616,12.574985"
//            var urlStr = "arcgis-navigator://?stop=\(stop)&travelmode=Trucking+Time"
//            urlStr += "&stopname=\("Final Point")"
//
//            let start = "41.831067,12.579537"
//            urlStr += "&start=\(start)&startname=\("Initial Point")"
            
            guard let str = urlStr.addingPercentEncoding(
               withAllowedCharacters: CharacterSet.urlQueryAllowed
               ),
               let url = URL(string: str) else {
                  fatalError()
               }
            
            if UIApplication.shared.canOpenURL(url) {
               UIApplication.shared.openURL(url)
            }
            else {
               self?.dependencies.messagesHandler.showInfo(
                  message: "Please, install the `Navigator for ArcGIS` app in the AppStore"
               )
               
               log.error("Unable to open url: \(url)")
            }
         }).disposed(by: disposeBag)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      navigationController?.setNavigationBarHidden(true, animated: true)
   }
}

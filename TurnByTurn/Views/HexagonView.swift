//
//  HexagonView.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/13/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit

//@IBDesignable
class HexagonView: UIView {
   @IBInspectable
   var strokeStart: CGFloat = 0
   
   @IBInspectable
   var strokeEnd: CGFloat = 1
   
   @IBInspectable
   var strokeWidth: CGFloat = 2
   
   @IBInspectable
   var strokeColor: UIColor = UIColor.black
   // Only override draw() if you perform custom drawing.
   // An empty implementation adversely affects performance during animation.
   override func draw(_ rect: CGRect) {
      // Drawing code
      let lineWidth: CGFloat = strokeWidth / 2
      guard let ctx = UIGraphicsGetCurrentContext() else { return }
      let path = UIBezierPath.createPolygon(rect: rect.insetBy(dx: lineWidth, dy: lineWidth), sides: 6)
      let layer = CAShapeLayer()
      layer.bounds = rect
      layer.path = path.cgPath
      layer.strokeStart = strokeStart / 2
      layer.strokeEnd = strokeEnd > strokeStart ? (strokeEnd / 2) : (strokeEnd / 2 + 0.5)
      
      layer.strokeColor = strokeColor.cgColor
      layer.fillColor = nil
      layer.lineWidth = lineWidth * 2
      layer.lineCap = kCALineCapRound
//      ctx?.draw(layer, in: layer.bounds)
      layer.render(in: ctx)
//      ctx.addPath(path.cgPath)
//      ctx.strokePath()
   }
   
}

extension UIBezierPath {
   static func createPolygon(rect: CGRect, sides: Int) -> UIBezierPath {
      let radiusX = rect.width / 2
      let radiusY = rect.height / 2
      
      let centerX = rect.midX
      let centerY = rect.midY
      
      let path = UIBezierPath()
      
      path.move(to: CGPoint(x: centerX + radiusX, y: centerY))
      
      for i in 0 ..< sides {
         let theta = 2 * CGFloat.pi / CGFloat(sides ) * CGFloat(i)
         let x = centerX + radiusX * cos(theta)
         let y = centerY + radiusY * sin(theta)
         path.addLine(to: CGPoint(x: x, y: y))
      }
      
      for i in 0 ..< sides {
         let theta = 2 * CGFloat.pi / CGFloat(sides ) * CGFloat(i)
         let x = centerX + radiusX * cos(theta)
         let y = centerY + radiusY * sin(theta)
         path.addLine(to: CGPoint(x: x, y: y))
      }
      
      path.close()
      return path
   }
}

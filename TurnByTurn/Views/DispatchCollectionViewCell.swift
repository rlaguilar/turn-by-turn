//
//  DispatchCollectionViewCell.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit
import RxSwift

class DispatchCollectionViewCell: UICollectionViewCell {
   @IBOutlet weak var incidentNumberLabel: UILabel!
   @IBOutlet weak var dispatchAddressLabel: UILabel!
   @IBOutlet weak var dispatchCityLabel: UILabel!
   @IBOutlet weak var dispatchTypeLabel: UILabel!
   @IBOutlet weak var dispatchDateLabel: UILabel!
   
   var reuseBag = DisposeBag()
   
   var item: Dispatch? {
      didSet {
         guard let item = self.item else { return }
         
         incidentNumberLabel.text = "\(item.id)"
         dispatchAddressLabel.text = item.addressFormated
         dispatchCityLabel.text = item.cityFormatted
         dispatchTypeLabel.text = item.typeFormatted
         dispatchDateLabel.text = item.dateFormatted
      }
   }
   
   override func prepareForReuse() {
      super.prepareForReuse()
      reuseBag = DisposeBag()
   }
}

extension Dispatch {
   var addressFormated: String {
      return (self.address ?? "missing address").uppercased(with: Locale.current)
   }
   
   var cityFormatted: String {
      if let city = self.city {
         return "@ \(city.uppercased(with: Locale.current))"
      }
      else {
         return "MISSING CITY"
      }
   }
   
   var typeFormatted: String { return (self.dispatchType ?? "MISSING TYPE").uppercased(with: Locale.current) }
   
   var dateFormatted: String { return self.createdAt.formatToReadable() }
}

//
//  DispatchCalloutView.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/13/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import UIKit
import RxSwift

class DispatchCalloutView: UIView {
   @IBOutlet weak var dispatchNumberLabel: UILabel!
   @IBOutlet weak var dispatchDateLabel: UILabel!
   @IBOutlet weak var dispatchAddress: UILabel!
   @IBOutlet weak var dispatchTypeLabel: UILabel!
   @IBOutlet weak var storyView: UITextView!
   
   private var disposable: Disposable?
   
   func update(item: Dispatch, api: ApiServiceType) {
      disposable?.dispose()
      
      dispatchNumberLabel.text = "\(item.id)"
      dispatchDateLabel.text = item.dateFormatted
      dispatchAddress.text = item.addressFormated
      dispatchTypeLabel.text = item.typeFormatted
      
      if let addressId = item.addressId {
         storyView.text = "Loading sizeup story..."
         
         disposable = api.getStory(forAddress: addressId)
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] sizeup in
               self.storyView.text = sizeup.story ?? "Missing story"
            })
         
         disposable?.disposed(by: disposeBag)
      }
      else {
         storyView.text = "Missing story"
      }
   }
}

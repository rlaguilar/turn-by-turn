//
//  LoginViewModel.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import Action
import RxCocoa
import RxSwift

class LoginViewModel {
   private let apiService: ApiServiceType
   
   let username = Variable("")
   let password = Variable("")
   
   lazy var login: Action<Void, UserCredentials> = {
      return Action<Void, UserCredentials>.init(
         enabledIf: Observable.combineLatest(
               username.asObservable(),
               password.asObservable()
            ) { user, pass -> Bool in
               return !user.isEmpty && !pass.isEmpty
            },
         workFactory: { [weak self] _  -> Observable<UserCredentials> in
            guard let `self` = self else {
               return .empty()
            }
            
            return self.apiService.login(
               email: self.username.value,
               password: self.password.value
            )
         })
   }()
   
   init(apiService: ApiServiceType) {
      self.apiService = apiService
   }
}

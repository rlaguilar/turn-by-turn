//
//  DispatchDetailsViewModel.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/10/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import CoreLocation

import Action
import RxCocoa
import RxSwift

class DispatchDetailsViewModel {
   let coordinates: CLLocationCoordinate2D?
   let sizeupUrl: URL?
   let dispatch: Dispatch
   
   let number = Variable("")
   let created = Variable("")
   let address = Variable("")
   let city = Variable("")
   
   let dispatchType: Variable<String?> = Variable(nil)
   let units: Variable<String?> = Variable(nil)
   let callNotes: Variable<String?> = Variable(nil)
   let buildingStory: Variable<String?> = Variable(nil)
   
   let disposeBag = DisposeBag()
   
   init(dispatch: Dispatch, apiService: ApiServiceType) {
      self.coordinates = dispatch.coordinates
      self.sizeupUrl = dispatch.url
      self.dispatch = dispatch
      
      number.value = "\(dispatch.id)"
      created.value = dispatch.dateFormatted
      address.value = dispatch.addressFormated
      city.value = dispatch.cityFormatted
      
      if let type = dispatch.dispatchType {
         self.dispatchType.value = type
      }
      
      log.debug(dispatch.dispatchUnits)
      
      if !dispatch.dispatchUnits.isEmpty {
         self.units.value = dispatch.dispatchUnits.joined(separator: ", ")
      }
      
      if let notes = dispatch.fullMessage {
         self.callNotes.value = notes
      }
      
      if let addressId = dispatch.addressId {
         self.buildingStory.value = "Loading building story"
         
         apiService.getStory(forAddress: addressId)
            .subscribe(onNext: { [unowned self] sizeupStory in
               self.buildingStory.value = sizeupStory.story
            }).disposed(by: disposeBag)
      }
   }
}

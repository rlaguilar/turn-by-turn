//
//  DispatchListViewModel.swift
//  TurnByTurn
//
//  Created by Reynaldo Aguilar on 11/9/17.
//  Copyright © 2017 Reynaldo Aguilar. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class DispatchListViewModel {
   private let disposeBag = DisposeBag()
   
   private let _latestDispatches = ReplaySubject<[Dispatch]>.create(bufferSize: 1)

   lazy var latestDispatches = self._latestDispatches.asObservable()
   
   init(dispatchFetcher: DispatchFetcher) {
      dispatchFetcher.timeline.subscribe(_latestDispatches)
         .disposed(by: disposeBag)
   }
}


class DispatchItemViewModel {
   let dispatch: Dispatch
   let isRead = Variable(false)
   
   init(dispatch: Dispatch) {
      self.dispatch = dispatch
   }
}
